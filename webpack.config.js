const webpack = require('webpack');
const path = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

module.exports = {
	entry:{
		main: './src/main.jsx'
	},

	output: {
		filename: '[name].bundle.js',
		path: path.resolve(__dirname, 'dist')
	},

	devtool: 'sourcemap',
	mode: 'development',

	module: {
		rules: [
			{
				test: /\.jsx?$/,
				exclude: /node_modules/,
				// Translate ES2015 to ES5
				loader: 'babel-loader',
				options: {
					presets: ['env','react']
				}
			},
			{
				test: /\.css$/,
				// Mark .css files to extract
				use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: "css-loader"
				})
				// exclude: /node_modules/,
				// use: ['style-loader', {
				// 	loader:'css-loader',
				// 	options: {
				// 		modules: true,
				// 		localIdentName: '[path][name]__[local]--[hash:base64:5]'
				// 	}
				// }]
			}
		]
	},
	plugins: [
		// Extract marked .css to separate 'styles.css' file
		new ExtractTextPlugin({
			filename: 'styles.css'
		}),
		// Get template for HTML file
		new HtmlWebpackPlugin({
			template: './src/index.html'
		}),
		// new UglifyJSPlugin()
	]
};
