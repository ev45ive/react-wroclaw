import { Action, ActionCreator } from "redux";
import { Todo } from "../models/todo";

export interface TodosState {
  entities: {
    [id: number]: Todo
  },
  todos: number[],
  archived: number[],
}

interface AddTodo extends Action {
  type: 'ADD_TODO', payload: Todo
}
interface RemoveTodo extends Action {
  type: 'REMOVE_TODO', payload: number
}
interface ToggleTodo extends Action {
  type: 'TOGGLE_TODO', payload: number
}
interface ArchiveTodo extends Action {
  type: 'ARCHIVE_TODO', payload: number
}
type TodosActions = AddTodo | RemoveTodo | ArchiveTodo | ToggleTodo

export const addTodo: ActionCreator<AddTodo> = (todo: Todo) => ({
  type: 'ADD_TODO', payload: todo
})

export const createTodo: ActionCreator<AddTodo> = (title: string) => ({
  type: 'ADD_TODO', payload: {
    id: Date.now(),
    title,
    completed: false
  }
})

export const removeTodo: ActionCreator<RemoveTodo> = (id: number) => ({
  type: 'REMOVE_TODO', payload: id
})
export const toggleTodo: ActionCreator<ToggleTodo> = (id: number) => ({
  type: 'TOGGLE_TODO', payload: id
})

export const archiveTodo: ActionCreator<ArchiveTodo> = (id: number) => ({
  type: 'ARCHIVE_TODO', payload: id
})

export const todos = (state: TodosState = {
  entities: {},
  archived: [],
  todos: []
}, action: TodosActions): TodosState => {

  switch (action.type) {
    case 'ADD_TODO':
      return {
        ...state,
        entities: {
          ...state.entities,
          [action.payload.id]: action.payload
        },
        todos: [...state.todos, action.payload.id]
      }
    case 'TOGGLE_TODO': {
        let todoId = action.payload,
            todo = state.entities[todoId]

        return {
          ...state,
          entities:{
            ...state.entities,
            [todoId]: {
              ...todo,
              completed: !todo.completed
            }
          }
        }
    }
    case 'REMOVE_TODO':
      return {
        ...state,
        todos: state.todos.filter(id => id !== action.payload),
        archived: state.archived.filter(id => id !== action.payload)
      }
    case 'ARCHIVE_TODO':
      let found = state.todos.find(id => id == action.payload)

      return found ? {
        ...state,
        //todos: state.todos.filter(id => id !== found),
        archived: [...state.archived, found]
      } : state;

    default: return state
  }
}

export const actions = {
  addTodo, removeTodo, createTodo, archiveTodo, toggleTodo
}

// getTodosList = createSelector(
//   state => state.todos,
//   state => state.enties,
//   (todos, entities) => todos.map(id => entities[id])
// )
// getTodosList(state)

export const getTodosList = (state: TodosState) => state.todos.map(id => state.entities[id])

export const getArchivedList = (state: TodosState) => state.archived.map(id => state.entities[id])