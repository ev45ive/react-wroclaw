import { Action, ActionCreator } from 'redux'

interface CounterAction extends Action {
  type: 'INC' | 'DEC'
  payload: number
}

export const inc: ActionCreator<CounterAction> = (payload = 1) => ({
  type: 'INC', payload
})

export const dec: ActionCreator<CounterAction> = (payload = 1) => ({
  type: 'DEC', payload
})

export const counter = (state: number = 0, action: CounterAction) => {
  switch (action.type) {
    case 'INC': return state + action.payload
    case 'DEC': return state - action.payload
    default: return state
  }
}