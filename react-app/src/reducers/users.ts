import { User } from "../models/user";
import { Action, ActionCreator } from "redux";

export interface UserState {
  users: User[],
  loading: boolean,
  error: string | null
}

interface LoadUsers extends Action {
  type: 'LOAD_USERS',
  payload: User[]
}
interface FetchUsers extends Action {
  type: 'FETCH_USERS',
}
// interface ErrorFetchingUsers extends Action {
//   type: 'ERROR_FETCHING_USERS',
// }
interface SaveUser extends Action {
  type: 'SAVE_USER',
  payload: User
}
type Actions = LoadUsers | SaveUser | FetchUsers


export const fetchUsers: ActionCreator<FetchUsers> = (dispatch) => {

  fetch('http://localhost:9000/users')
    .then(response => response.json())
    .then(users => {
      dispatch({ type: 'LOAD_USERS', payload: users })
    })
    .catch((error) => {
      dispatch({ type: 'ERROR_FETCHING_USERS', payload: error.message })
    })
  return { type: 'FETCH_USERS' }
}

export const users = (state: UserState = {
  users: [], error: null, loading: false
}, action: Actions): UserState => {

  switch (action.type) {
    case 'LOAD_USERS':
      return {
        ...state,
        users: action.payload,
        loading: false
      }
    case 'FETCH_USERS':
      return {
        ...state,
        loading: true
      }
    default: return state
  }
}

export const getUsersList = (state:UserState) => state.users

export const getUsersLoading = (state:UserState) => state.loading

export const getUserById = (state:UserState, id: string) => state.users.find(user => user.id == parseInt(id))