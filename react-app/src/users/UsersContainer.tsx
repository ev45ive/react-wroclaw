import { connect } from "react-redux";
import { Users } from "./Users";
import { AppState } from "../store";
import { getUsersList, getUsersLoading, fetchUsers } from "../reducers/users";
import { bindActionCreators } from "redux";


export const UsersContainer = connect(
  (state:AppState) => ({
    users: getUsersList(state.users),
    loading: getUsersLoading(state.users)
  }),
  dispatch => bindActionCreators({
      fetchUsers: () => fetchUsers(dispatch)
  },dispatch),
  (state, dispatch, own) => ({
    ...state, ...dispatch, ...own
  })
)(Users)