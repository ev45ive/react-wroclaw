import React from 'react'
import { User } from '../models/user';
import { Route, RouteComponentProps } from 'react-router';
import { UserFormContainer } from './UserFormContainer';
// import { UserForm } from './UserForm';

interface Props extends RouteComponentProps<{id:string}> {
  users: User[],
  loading: boolean,
  fetchUsers: () => any
} 

export class Users extends React.PureComponent<Props>{

  componentDidMount() {
    this.props.fetchUsers()
  }

  select = (selected: User) => {
    this.props.history.replace('/users/'+selected.id)
  }

  // onSave = (user:User) => {
  //   fetch('http://localhost:9000/users/'+ user.id,{
  //     method:'PUT',
  //     body: JSON.stringify(user),
  //     headers:{
  //       'Content-Type':'application/json'
  //     }
  //   })
  //   .then( () => this.fetchUsers() )
  //   .then( () => {
  //     let found = this.state.users.find( ({id}) => id == user.id)
  //     if(found)
  //     this.select(found)
  //   })
  // }

  render() {
    return <div className="row">
      <div className="col">
        <h2>Users List</h2>

        {this.props.loading ? 'Loading ...' :
          <table className="table table-striped table-hover">
            <thead>
              <tr>
                <th>Name</th>
                <th>E-Mail</th>
              </tr>
            </thead>
            <tbody>
              {this.props.users.map(user =>
                <tr key={user.id}
                  onClick={() => this.select(user)}
                // className={
                //   this.state.selected && this.state.selected.id == user.id ? 'table-active' : ''
                // }
                >
                  <td>{user.name}</td>
                  <td>{user.email}</td>
                </tr>
              )}
            </tbody>
          </table>
        }

      </div>
      <div className="col">
        <h2>User Form</h2>

        <Route path="/users/:id" render={(props) => {

          return <UserFormContainer selectedId={props.match.params['id']} />
        }
        } />
          {/* {this.state.selected? 
        <UserForm user={ this.state.selected } onSave={this.onSave} /> : 'Please select user' } */}
      </div>
    </div>

      }
}