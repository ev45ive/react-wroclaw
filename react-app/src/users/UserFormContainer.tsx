import { connect } from "react-redux";
import { UserForm } from "./UserForm";
import { AppState } from "../store";
import { UserState, getUserById } from "../reducers/users";
import { User } from "../models/user";


// <UserFormContainer selectedId="???"
export const UserFormContainer = connect(
  (state: AppState,  props:{selectedId:string}) => ({
    user: getUserById(state.users,props.selectedId),
    onSave: () => { }
  })
)(UserForm)