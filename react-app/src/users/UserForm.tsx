import React from 'react'
import { User } from '../models/user';

interface State{
  user?: User
}
interface Props{
  user: User | undefined
  onSave: (user:User) => void
}
export class UserForm extends React.Component<Props, State>{

  state:State = {
    user: this.props.user
  }

  componentWillReceiveProps(props:Props){
    this.setState({
      user: props.user
    })
  }

  handleField = (event: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>) =>{
    const elem = event.currentTarget,
          name = elem.name,
          value = elem.value;

    this.setState( state => ({
      user: state.user? {
        ...state.user,
        [name]: value
      } : undefined
    }))
  }

  render(){
    return this.state.user? <form onSubmit={ event => event.preventDefault() }>
      <div className="form-group">
        <label htmlFor="name">Name</label>
        <input type="text" id="name" className="form-control" 
                                      name="name"
                                      onChange={this.handleField}
                                      value={this.state.user.name || ''}/>
      </div>
      <div className="form-group">
        <label>E-mail</label>
        <input type="text" className="form-control" 
                                      name="email"
                                      onChange={this.handleField}
                                      value={this.state.user.email || ''}/>
      </div>
      <div className="form-group">
        <input type="submit" className="btn btn-success" value="Save"
          onClick={() => this.props.onSave(this.state.user)}/>
      </div>
    </form> : 'Select User'
  }
}