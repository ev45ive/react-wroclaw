import { createStore, combineReducers } from 'redux'
import { todos, TodosState } from './reducers/todos';
import { users, UserState } from './reducers/users';

export interface AppState{
  todos: TodosState,
  users: UserState,
};

const rootReducer = combineReducers<AppState>({
  todos,
  users
})

export const store = createStore<AppState>(rootReducer)

window['store'] = store



// const rootReducer = (state:AppState, action:AnyAction) => {
//   return {
//     ...state,
//     counter: counterReducer(state.counter,<CounterAction>action)
//     X: XReducer(state.X,<XAction>action)
//     Y: YReducer(state.Y,<YAction>action)
//   }
// }