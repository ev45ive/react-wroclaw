export interface Todo{
  id: number
  title:string
  /**
   * Is todo completed
   */
  completed?:boolean
}