import { connect } from 'react-redux'
import { Todos } from './Todos'
import { AppState } from '../store';
import { getTodosList, actions } from '../reducers/todos';
import { bindActionCreators } from 'redux';

export const TodosContainer = connect(
  // mapStateToProps
  (state: AppState) => ({
    title: 'Awesome',
    todos: getTodosList(state.todos)
  }),
  // mapDispatchToProps
  dispatch => bindActionCreators({
    onRemove: actions.archiveTodo,
    onToggle: actions.toggleTodo
  }, dispatch)
)(Todos)
