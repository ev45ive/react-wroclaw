import * as React from 'react';
import { Todo } from '../models/todo';

export const TodosList = (props: {
  title: string,
  todos: Todo[],
  onRemove: (todoId: number) => void,
  onToggle: (todoId: number) => void
}) => <div className="list-group">
    {
      props.todos.map(todo => <div
        key={todo.id}
        className="list-group-item">
        
        <input type="checkbox" 
        onChange={()=>props.onToggle(todo.id)}  checked={todo.completed} />
        
        {todo.title}

        <span className="close" onClick={ 
          () => props.onRemove( todo.id )
        }> &times; </span>

      </div>)
    }
  </div>