import React, { PureComponent } from 'react'
import { TodosList } from './TodosList';
import { Todo } from '../models/todo';

export class Todos extends PureComponent<{
  todos: Todo[],
  title: string,
  onRemove: (todoId: number) => any,
  onToggle: (todoId: number) => any
}> {

  static defaultProps = {
    title: 'Todos'
  }

  // componentWillReceiveProps( newProps = {} ){
  //   console.log('componentWillReceiveProps',newProps)
  // }

  // shouldComponentUpdate( newProps={}, newState={} ){
  //   return newProps['todos'] !== this.props['todos']
  // }
  // or PureComponent

  render() {
    // console.log(this.props.title, 'render')
    return (
      <div>
        <h3>{this.props.title}</h3>
        <TodosList
          onRemove={this.props.onRemove}
          onToggle={this.props.onToggle}
          title="Todos"
          todos={this.props.todos} />
      </div>
    )
  }
}