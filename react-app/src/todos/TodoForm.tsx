import React, { Component } from 'react'

interface State {
  newTodoTitle: string
}

export class TodoForm extends Component<{
  onNewTodo: (todoTitle:string) => any
}, State>{

  state: State = {
    newTodoTitle: ''
  }

  inputChange = (event: React.FormEvent<HTMLInputElement>) => {
    const newTodoTitle = event.currentTarget.value;

    this.setState({
      newTodoTitle
    })
  }

  onKeyUp = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if(event.key == "Enter"){
      this.addTodo()
    }
  }

  addTodo = () => {
    this.props.onNewTodo(this.state.newTodoTitle)
    this.setState({
      newTodoTitle:''
    })
  }

  // componentDidMount(){
  //   console.log(this)
  //   ;(this.refs['formInputRef'] as HTMLInputElement).focus()
  // }

  componentWillUnmount(){

  }

  render() {
    return (
      <div className="input-group">
        <input type="text" className="form-control"
          // ref="formInputRef"
          ref={ elem => elem && elem.focus() }
          value={this.state.newTodoTitle}
          onChange={this.inputChange}
          onKeyUp={this.onKeyUp} />
        <button className="btn" onClick={this.addTodo}>Add</button>
      </div>
    )
  }
}