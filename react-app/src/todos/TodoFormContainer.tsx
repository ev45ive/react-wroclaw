import { connect } from 'react-redux'
import { actions } from '../reducers/todos';
import { TodoForm } from './TodoForm';

export const TodoFormContainer = connect(
  // mapStateToProps
  null,
  // mapDispatchToProps
  (dispatch) => ({
    onNewTodo: (todoTitle:string) => {
      dispatch(actions.createTodo(todoTitle))
    },
  })
)(TodoForm)
