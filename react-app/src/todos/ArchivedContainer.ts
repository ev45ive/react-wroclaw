import { connect } from 'react-redux'
import { Todos } from './Todos'
import { AppState } from '../store';
import { actions, getArchivedList } from '../reducers/todos';
import { bindActionCreators } from 'redux';

export const ArchivedContainer = connect(
  (state: AppState) => ({
    title: 'Archived',
    todos: getArchivedList(state.todos)
  }),
  (dispatch) => bindActionCreators({
    onRemove: actions.removeTodo,
    onToggle: actions.toggleTodo
  }, dispatch)
  // mergeProps 
  // (stateProps, dispatchProps, ownProps) => {
  //   debugger
  //   return ({...stateProps, ...dispatchProps})
  // }
)(Todos)
