import React from 'react'

export const Tab = (props: {
  name: string,
  children: JSX.Element
}) => props.children;

export class Tabs extends React.Component {

  state = {
    tab: 'users'
  }

  render() {
    console.log(this.props.children)

    return <div>
      {
        React.Children.toArray(this.props.children).filter(
          elem => (elem as JSX.Element).props.name == this.state.tab
        ).map(elem => (elem as JSX.Element).props.children)
      }
    </div>
  }
}