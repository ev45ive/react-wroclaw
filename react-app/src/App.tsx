import React from 'react';
import './App.css';
import { TodosContainer } from './todos/Todos.container';
import { TodoFormContainer } from './todos/TodoFormContainer';
import { ArchivedContainer } from './todos/ArchivedContainer';
import { UsersContainer } from './users/UsersContainer';

import { Route, Redirect, Switch } from 'react-router';
import { NavLink } from 'react-router-dom';

interface AppState {
  tab: string
}

class App extends React.Component<{}, AppState> {

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col">
            <ul className="nav nav-pills">
              <li className="nav-item">
                <NavLink to="/users" className="nav-link" activeClassName="active"> Users</NavLink>
              </li>
              <li className="nav-item">
                <NavLink to="/todos" className="nav-link" activeClassName="active"> Todos</NavLink>
              </li>
            </ul>
          </div>
        </div>
        <div className="row">
          <div className="col">

            <Switch>
              <Route path="/" exact render={() => <Redirect to="/users" /> } />

              <Route path="/users" component={UsersContainer} />

              <Route path="/todos" render={() => <div className="row">
                <div className="col">
                  <TodosContainer />

                  <TodoFormContainer />
                </div>
                <div className="col">
                  <ArchivedContainer />
                </div>
              </div>} />

              <Route render={() => 'Page not found' } />

            </Switch>

          </div>
        </div>
      </div>
    );
  }
}

export default App;
