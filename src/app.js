import styles from './app.css'
// console.log(styles.testClass)

export class App{

  constructor(){
    this.version = '1.5.0'
  }

  run(){
    console.log('Version '+ this.version)
  }
}

export var config = {}

export default App