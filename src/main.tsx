import * as React from 'react'
import * as ReactDOM from 'react-dom'

import { App } from './app'

let index = 1, items = []

const Todos = props => <section>
  <h3 style={{ color: 'red' }}>{props.title}</h3>
  <ul className="list-group">
    {
      props.todos.map(item => <li key={item.title} className="list-group-item">
        {item.title}
      </li>)
    }
  </ul>
</section>

setInterval(() => {
  items.unshift({
    title: 'Item ' + index++
  })

  ReactDOM.render(<Todos todos={items} title="Todos" />, document.getElementById('root'))
}, 500)
